<?php

use Kisphp\XmlParser;

class XmlParserTest extends PHPUnit_Framework_TestCase
{
    protected $tree;

    public function setUp()
    {
        $xml = file_get_contents(__DIR__ . '/testfile.xml');
        $x = new XmlParser();
        $x->parseString($xml);

        $this->tree = $x->getTree();
    }

    public function test_MsgId()
    {
        $xml_body = file_get_contents(__DIR__ . '/testCdata.xml');

        $x = new XmlParser();
        $x->parseString($xml_body);

        $tree = $x->getTree();

        $this->assertArrayHasKey('MsgId', $tree['xml']);

        $this->assertEquals('12345678987654321', $tree['xml']['MsgId']);
    }

    public function test_Voice_MsgId()
    {
        $xml_body = file_get_contents(__DIR__ . '/testCdata.xml');

        $x = new XmlParser();
        $x->parseString($xml_body);

        $tree = $x->getTree();

        $this->assertArrayHasKey('ToUserName', $tree['xml']);

        $this->assertEquals('toUser', $tree['xml']['ToUserName']);

        $this->assertEquals('media_id', $tree['xml']['Voice']['MediaId']);
    }

    public function test_is_nba()
    {
        $this->assertTrue( isset($this->tree['nba']) );
    }

    public function test_is_nba_team()
    {
        $this->assertTrue( isset($this->tree['nba']['team']) );
    }

    public function test_nba_team()
    {
        $this->assertGreaterThan(0, count($this->tree['nba']['team']));
    }

    public function test_nba_team_content()
    {
        $this->assertEquals(2, count($this->tree['nba']['team'][0]));
    }
}
