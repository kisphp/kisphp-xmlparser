<?php

namespace Kisphp;

use Kisphp\XmlParseException;

class XmlParser
{
    /**
     * resource could not be created
     * @todo remove
     */
    const XML_PARSER_ERROR_NO_RESOURCE = 200;

    /**
     * unsupported mode
     * @todo remove
     */
    const XML_PARSER_ERROR_UNSUPPORTED_MODE = 201;

    /**
     * invalid encoding was given
     * @todo remove
     */
    const XML_PARSER_ERROR_INVALID_ENCODING = 202;

    /**
     * specified file could not be read
     * @todo remove
     */
    const XML_PARSER_ERROR_FILE_NOT_READABLE = 203;

    /**
     * invalid input
     * @todo remove
     */
    const XML_PARSER_ERROR_INVALID_INPUT = 204;

    /**
     * remote file cannot be retrieved in safe mode
     */
    const XML_PARSER_ERROR_REMOTE = 205;

    protected $parser = null;

    protected $sourceEncoding = null;

    protected $destinationEncoding = null;

    protected $_struct = array();

    public function __construct($sourceEncoding = null, $destinationEncoding = null)
    {
        $this->sourceEncoding = $sourceEncoding;
        $this->destinationEncoding = $destinationEncoding;
    }

    protected function free()
    {
        if ( isset($this->parser) && is_resource($this->parser) ) {
            xml_parser_free($this->parser);
            unset($this->parser);
        }
    }

    public function parseFile($file)
    {
        if ( is_file($file) ) {
            $data = @file_get_contents($file);
            $this->parseString($data);
        } else {
            throw new \Exception('File ' . $file . ' does not exists!');
        }
    }

    public function parseString($data)
    {
        if ( $this->sourceEncoding === null ) {
            $this->parser = @xml_parser_create();
        } else {
            $this->parser = @xml_parser_create($this->sourceEncoding);
        }

        if ( ! $this->parser ) {
            throw new XmlParseException(
                'Unable to create XML parser resource ' . ( $this->sourceEncoding === null )
                    ? 'with '. $this->sourceEncoding .' encoding.'
                    : ''
            );
        }

        if ( $this->destinationEncoding !== null ) {
            if ( ! @xml_parser_set_option($this->parser, XML_OPTION_TARGET_ENCODING, $this->destinationEncoding) ) {
                throw new XmlParseException('Invalid target encoding');
            }
        }

        xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, 0);   // lowercase tags
        xml_parser_set_option($this->parser, XML_OPTION_SKIP_WHITE, 1);     // skip empty tags

        if ( ! xml_parse_into_struct($this->parser, $data, $this->_struct)) {
            $error = sprintf('XML Error: %s at line %d',
                xml_error_string(xml_get_error_code($this->parser)),
                xml_get_current_line_number($this->parser)
            );
            $this->free();
            throw new XmlParseException($error);
        }

        $this->_count = count($this->_struct);
        $this->free();
    }

    public function getTree()
    {
        $i = 0;
        $tree = array();

        $tree = $this->addNode(
            $tree,
            $this->_struct[$i]['tag'],
            ( isset($this->_struct[$i]['value']) ) ? $this->_struct[$i]['value'] : '',
            ( isset($this->_struct[$i]['attributes']) ) ? $this->_struct[$i]['attributes'] : '',
            $this->getChild($i)
        );

        unset($this->_struct);
        return ($tree);
    }

    protected function getChild(&$i)
    {
        $children = array();

        while (++$i < $this->_count) {
            // node tag name
            $tagname = $this->_struct[$i]['tag'];
            $value = isset($this->_struct[$i]['value']) ? $this->_struct[$i]['value'] : '';
            $attributes = isset($this->_struct[$i]['attributes']) ? $this->_struct[$i]['attributes'] : '';

            switch ($this->_struct[$i]['type']) {

                case 'open':
                    // node has more children
                    $child = $this->getChild($i);
                    // append the children data to the current node
                    $children = $this->addNode($children, $tagname, $value, $attributes, $child);
                    break;

                case 'complete':
                    // at end of current branch
                    $children = $this->addNode($children, $tagname, $value, $attributes);
                    break;

                case 'cdata':
                    // node has CDATA after one of it's children
                    $children['value'] .= $value;
                    break;

                case 'close':
                    // end of node, return collected data
                    return $children;
                    break;
            }

        }
    }

    protected function addNode($target, $key, $value = '', $attributes = '', $child = '')
    {
        if ( ! isset($target[$key]['value']) && ! isset($target[$key][0]) ) {
            if ( $child != '' ) {
                $target[$key] = $child;
            }
            if ( $attributes != '' ) {
                foreach ($attributes as $k => $v) {
                    $target[$key][$k] = $v;
                }
            }
            if ( ! empty($value) ) {
                if ( empty($attributes) ) {
                    $target[$key] = $value;
                } else {
                    $target[$key]['value'] = $value;
                }
            }
        } else {
            if ( ! isset($target[$key][0]) ) {
                // is string or other
                $oldvalue = $target[$key];
                $target[$key] = array();
                $target[$key][0] = $oldvalue;
                $index = 1;
            } else {
                // is array
                $index = count($target[$key]);
            }

            if ( $child != '' ) {
                $target[$key][$index] = $child;
            }

            if ( $attributes != '' ) {
                foreach ($attributes as $k => $v) {
                    $target[$key][$index][$k] = $v;
                }
            }
            if ( ! empty($value) ) {
                $target[$key][$index]['value'] = $value;
            }
        }
        return $target;
    }
}
