<?php

namespace Kisphp;

class XmlParseException extends \Exception
{
    /**
     * construct a new error instance
     *
     * You may either pass a message or an xml_parser resource as first
     * parameter. If a resource has been passed, the last error that
     * happened will be retrieved and returned.
     *
     * @param string|resource $msgOrParser message or parser resource
     * @param integer         $code        error code
     *
     * @access   public
     */
    public function __construct($msgOrParser = 'unknown error', $code = 0)
    {
        if ( is_resource($msgOrParser) ) {
            $code        = xml_get_error_code($msgOrParser);
            $msgOrParser = sprintf('%s at XML input line %d:%d',
                xml_error_string($code),
                xml_get_current_line_number($msgOrParser),
                xml_get_current_column_number($msgOrParser)
            );
        }

        parent::__construct($msgOrParser, $code);
    }
}
